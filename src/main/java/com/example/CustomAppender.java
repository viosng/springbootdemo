package com.example;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

/**
 * Created by vio on 31.10.2015.
 */
@Plugin(category = "Core", name = "CustomAppender", elementType = "appender")
public class CustomAppender extends AbstractAppender {

    @Autowired
    Printer printer;

    protected CustomAppender(String name, Filter filter, Layout<? extends Serializable> layout) {
        super(name, filter, layout);
    }

    @PluginFactory
    public static CustomAppender createAppender(@PluginAttribute("name") String name) {
        return new CustomAppender(name, null, null);
    }

    @Override
    public void append(LogEvent arg0) {
        printer.print("[>>>" + arg0.getLevel() + "<<<]" + arg0.getMessage().getFormattedMessage());
    }

}

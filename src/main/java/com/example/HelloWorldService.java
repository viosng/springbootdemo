package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by vio on 18.10.2015.
 */
@Component
public class HelloWorldService {

    public static final Marker SQL_MARKER = MarkerFactory.getMarker("SQL");
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Value("${name:World}")
    private String name;

    public String getHelloMessage() {
        log.debug(SQL_MARKER, "123 : {}", "markered");
        log.warn(SQL_MARKER, "123 : {}", "markered");
        log.info(SQL_MARKER, "123 : {}", "markered");
        log.error(SQL_MARKER, "123 : {}", "markered");
        log.trace(SQL_MARKER, "123 : {}", "markered");
        log.info("not markered");
        return "Hello " + this.name;
    }

}
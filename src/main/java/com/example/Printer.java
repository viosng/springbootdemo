package com.example;

import org.springframework.stereotype.Service;

/**
 * Created by vio on 31.10.2015.
 */
@Service
public class Printer {
    public void print(String s) {
        System.out.println(s);
    }
}

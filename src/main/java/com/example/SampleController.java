package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by vio on 18.10.2015.
 */
@Controller
public class SampleController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private HelloWorldService helloWorldService;

    @RequestMapping("/")
    @ResponseBody
    public String helloWorld() {
        logger.info("123");
        return this.helloWorldService.getHelloMessage();
    }

}

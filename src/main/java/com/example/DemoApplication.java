package com.example;


import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.filter.MarkerFilter;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableBatchProcessing
public class DemoApplication extends SpringBootServletInitializer {

    @PostConstruct
    private void initLogger() {
        LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        final Configuration config = ctx.getConfiguration();
        CustomAppender appender = customAppender();
        config.addAppender(appender);
        appender.start();
        LoggerConfig loggerConfig = config.getLoggerConfig("");

        loggerConfig.addAppender(appender, Level.DEBUG, MarkerFilter.createFilter("SQL", Filter.Result.ACCEPT, Filter.Result.DENY));
        ctx.updateLoggers();
    }

    @Bean
    public CustomAppender customAppender() {
        return CustomAppender.createAppender("abc");
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(DemoApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(DemoApplication.class, args);
    }
}

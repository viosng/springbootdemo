package com.example;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.OutputCapture;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by vio on 18.10.2015.
 */
public class SampleBatchTest {
    @Rule
    public OutputCapture outputCapture = new OutputCapture();

    @Test
    public void testDefaultSettings() throws Exception {
        assertEquals(0, SpringApplication
                .exit(SpringApplication.run(DemoApplication.class)));
        String output = this.outputCapture.toString();
        assertTrue("Wrong output: " + output,
                output.contains("completed with the following parameters"));
    }
}
